package spectrumanalizer;

public class Dot {
    private double x, y;
    
    public Dot() {
        super();
    }
    
    public Dot (double xvalue, double yvalue) {
        x = xvalue;
        y = yvalue;
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    public void setX(double xvalue) {
        x = xvalue;
    }
    
    public void setY(double yvalue) {
        y = yvalue;
    }
    
    @Override
    public boolean equals(Object dot) {
        // TODO Auto-generated method stub
        Dot otherDot = (Dot)dot;
        return (otherDot.x == x) && (otherDot.y == y);
    }
    
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return Math.round((float)(x+y));
    }
    
    public void print() {
        System.out.println("(" + x + "; " + y + ")");
    }
}
