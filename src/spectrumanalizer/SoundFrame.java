package spectrumanalizer;

import edu.emory.mathcs.jtransforms.dct.DoubleDCT_1D;

public class SoundFrame {
    private double[] samples;
    private double[] spectrum;
    
    public static int RATE = 44100;

    public SoundFrame(double[] samples) {
        /* DoubleFFT_1D transformer; */
        DoubleDCT_1D transformer;

        this.samples = samples.clone();
        this.spectrum = samples.clone();

        /*
         * transformer = new DoubleFFT_1D(samples.length);
         * transformer.realForward(this.spectrum);
         */
        transformer = new DoubleDCT_1D(samples.length);
        transformer.forward(spectrum, true);

        System.out.println(this.spectrum.length);
    }
    
    @Deprecated
    public double[][] getSamples() {
        double[][] actSamples;
        
        actSamples = new double[samples.length][2];
        
        for (int i = 0; i < samples.length; i++) {
            actSamples[i][0] = (double) i / SoundFrame.RATE;
            actSamples[i][1] = samples[i];
        }
        
        return actSamples;
    }

    public DotSet getDotSamples() {
        DotSet actSamples;
        Dot currDot;
        
        actSamples = new DotSet();

        for (int i = 0; i < samples.length; i++) {
            currDot = new Dot();
            currDot.setX((double) i / SoundFrame.RATE);
            currDot.setY(samples[i]);
            
            actSamples.add(currDot);
        }

        return actSamples;
    }

    
    @Deprecated
    public double[][] getSpectrum() {
        double[][] actSpectrum;
        
        actSpectrum = new double[spectrum.length / 2][2];
        
        for (int i = 0; i < samples.length / 4; i++) {
            actSpectrum[i][0] = i * SoundFrame.RATE / samples.length;
            actSpectrum[i][1] = spectrum[2 * i + 1];
        }
        
        return actSpectrum;
    }
    
    public DotSet getDotSpectrum() {
        //double[][] actSpectrum;
        DotSet actSpectrum;
        Dot currDot;

        //actSpectrum = new double[spectrum.length / 2][2];
        actSpectrum = new DotSet();
        
        for (int i = 0; i < samples.length / 4; i++) {
            currDot = new Dot();
            currDot.setX(i * SoundFrame.RATE / samples.length);
            currDot.setY(spectrum[2 * i + 1]);
            
            actSpectrum.add(currDot);
        }
        
        return actSpectrum;
    }
    
    public SoundFrame add(SoundFrame sf) throws Exception {
        SoundFrame ret;
        double[] samples;
        int i;
        
        if (sf.samples.length != this.samples.length) {
            throw new Exception("Lunghezze di frame diverse");
        }

        samples = new double[sf.samples.length];
        
        for (i = 0; i < sf.samples.length; i++) {
            samples[i] = sf.samples[i] + this.samples[i];
        }
        
        ret = new SoundFrame(samples);
        
        return ret;
    }
}