package spectrumanalizer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Iterator;

import javax.swing.JPanel;

public class GraphDrawer2 extends JPanel {
    /*int[] data = {
        21, 14, 18, 03, 86, 88, 74, 87, 54, 77,
        61, 55, 48, 60, 49, 36, 38, 27, 20, 18
    };*/
    private DotSet data;
    final int PAD = 20;
    
    public GraphDrawer2(DotSet ds) {
        data = ds;
    }
    
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
        int w = getWidth();
        int h = getHeight();
        // Draw ordinate.
        g2.draw(new Line2D.Double(PAD, PAD, PAD, h-PAD));
        // Draw abcissa.
        g2.draw(new Line2D.Double(PAD, (h/2)-PAD, w-PAD, (h/2)-PAD));
        /*double xInc = (double)(w - 2*PAD)/(data.length-1);
        double scale = (double)(h - 2*PAD)/getMax();*/
        
        double xInc = (double)(w - 2*PAD)/data.getMaxX();
        double scale = (double)(h - 2*PAD)/data.getMaxY();
        // Mark data points.
        g2.setPaint(Color.red);
        Iterator<Dot> iter;
        Dot currDot;
        
        iter = data.iterator();
        
        while (iter.hasNext()) {
            double x, y;
            currDot = iter.next();
            
            x = PAD + xInc*currDot.getX();
            y = (h/2) - PAD - (scale/2)*currDot.getY();
            
            g2.fill(new Ellipse2D.Double(x-2, y-2, 1, 1));
            //g2.draw(new Line2D.Double(x, y, x, h-PAD));
        }
        /*
        
        for(int i = 0; i < data.length; i++) {
            double x = PAD + i*xInc;
            double y = h - PAD - scale*data[i];
            g2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));
            g2.draw(new Line2D.Double(x, y, x, h-PAD));
        }
        */
    }
    
    /*private int getMax() {
        int max = -Integer.MAX_VALUE;
        for(int i = 0; i < data.length; i++) {
            if(data[i] > max)
                max = (int)Math.round(data[i]);
        }
        return max;
    }*/
}