package spectrumanalizer;

public class WaveUtils {
    public static SoundFrame generateSin(double frequency, int usec,
            int periodSamples) {
        double[] ret;
        int i, size;

        size = (int) (((double) (usec) / 1000000) * periodSamples);

        System.out.println("asfasfQW" + size);

        ret = new double[size];

        for (i = 0; i < size; i++) {
            ret[i] = i * frequency * 2 * Math.PI;
            ret[i] /= periodSamples;
            ret[i] = Math.sin(ret[i]);
        }
        
        return new SoundFrame(ret);
    }

    public static SoundFrame generateCos(double frequency, int usec,
            double periodSamples) {
        double[] ret;
        int i, size;

        size = (int) (((double) (usec) / 1000000) * periodSamples);

        ret = new double[size];

        for (i = 0; i < size; i++) {
            ret[i] = i * frequency * 2 * Math.PI;
            ret[i] /= periodSamples;
            ret[i] = Math.cos(ret[i]);
        }
        
        return new SoundFrame(ret);
    }

    public static SoundFrame generateSquare(double frequency, int usec,
            double periodSamples) {
        double[] ret;
        int i, size;

        size = (int) (((double) (usec) / 1000000) * periodSamples);

        ret = new double[size];
        
        for (i = 0; i < size; i++) {
            if (i % (periodSamples/frequency) <= (periodSamples/(2*frequency))) {
                ret[i] = -1;
            }
            else {
                ret[i] = 1;
            }
        }
        
        return new SoundFrame(ret);
    }

    public static SoundFrame generateSaw(double frequency, int usec,
            double periodSamples) {
        double[] ret;
        int i, size;
        
        size = (int) (((double) (usec) / 1000000) * periodSamples);

        ret = new double[size];
        
        for (i = 0; i < size; i++) {
            ret[i] = (i % (periodSamples/frequency)) * ((2*frequency)/periodSamples) - 1;
        }
        return new SoundFrame(ret);
    }
}
