package spectrumanalizer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.jfree.ui.RefineryUtilities;

public class Main {

    @Deprecated
    public static void viewOld(SoundFrame sf) {
        GraphDrawer g;
        g = new GraphDrawer();

        g.printDouble(sf.getSamples(), "Samples", sf.getSpectrum(), "Spectrum");
        g.setSize(1000, 500);
        g.setVisible(true);
        g.setFocusable(true);

        RefineryUtilities.centerFrameOnScreen(g);
    }
    
    public static void view(SoundFrame sf) {
        JFrame frame = new JFrame();
        
        JPanel f = new JPanel();
        GraphDrawer2 g1, g2;
        
        g1 = new GraphDrawer2(sf.getDotSamples());
        g1.setSize(400, 400);
        g2 = new GraphDrawer2(sf.getDotSpectrum());
        sf.getDotSpectrum().print();
        g2.setSize(400, 400);
        
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(new BoxLayout(f, BoxLayout.Y_AXIS));
        //f.add(new GraphDrawer2(x));
        f.add(g1);
        f.add(g2);
        f.setSize(400,800);
        //f.setLocation(200,200);
        //f.setVisible(true);
        
        frame.add(f);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 800);
    }
    
    public static void main(String[] args) throws Exception {
        
        SoundFrame sf;

        sf = WaveUtils.generateSin(100, 100000, SoundFrame.RATE);
        sf = sf.add(WaveUtils.generateSaw(100, 100000, SoundFrame.RATE));
        sf = sf.add(WaveUtils.generateSquare(100, 100000, SoundFrame.RATE));
        
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Old but still workin' ^^
        viewOld(sf);
        
        //view(sf);
        
    }
}