package spectrumanalizer;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.util.ShapeUtilities;

public class GraphDrawer extends ApplicationFrame {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private ChartPanel chartpanel;
    private JFreeChart chart;
    XYSeriesCollection xyDataset;

    public GraphDrawer() {
        super("My drawer :O");
    }

    public void printSingle(double[][] dots, String name) {
        XYItemRenderer dotRenderer;
        XYSeries xySeries;

        xySeries = new XYSeries(name);

        for (int i = 0; i < dots.length; i++) {
            xySeries.add(dots[i][0], dots[i][1]);
        }

        xyDataset = new XYSeriesCollection();
        xyDataset.addSeries(xySeries);

        chart = ChartFactory.createScatterPlot("Default", "X", "Y", xyDataset,
                PlotOrientation.VERTICAL, true, true, true);

        dotRenderer = ((XYPlot) chart.getPlot()).getRenderer();

        dotRenderer.setSeriesPaint(0, Color.RED);

        ((AbstractRenderer) dotRenderer).setSeriesShape(0,
                ShapeUtilities.createDiamond(1), true);

        chartpanel = new ChartPanel(chart);
        chartpanel.setPreferredSize(new Dimension(500, 500));
        chartpanel.setMouseZoomable(true);
        chartpanel.setMouseWheelEnabled(true);

        setContentPane(chartpanel);
    }

    public void printDouble(double[][] dots1, String name1, double[][] dots2,
            String name2) {
        XYItemRenderer dotRenderer1, dotRenderer2;
        XYPlot xyPlot1, xyPlot2;
        XYSeries xySeries1, xySeries2;
        XYSeriesCollection xyDataset1, xyDataset2;
        JFreeChart chart1, chart2;
        ChartPanel chartpanel1, chartpanel2;
        JPanel jPanel;

        xySeries1 = new XYSeries(name1);
        xySeries2 = new XYSeries(name2);

        for (int i = 0; i < dots1.length; i++) {
            xySeries1.add(dots1[i][0], dots1[i][1]);
        }

        for (int i = 0; i < dots2.length; i++) {
            xySeries2.add(dots2[i][0], dots2[i][1]);
        }

        xyDataset1 = new XYSeriesCollection();
        xyDataset2 = new XYSeriesCollection();

        xyDataset1.addSeries(xySeries1);
        xyDataset2.addSeries(xySeries2);

        chart1 = ChartFactory.createScatterPlot("Default", "X", "Y",
                xyDataset1, PlotOrientation.VERTICAL, true, true, true);
        chart2 = ChartFactory.createScatterPlot("Default", "X", "Y",
                xyDataset2, PlotOrientation.VERTICAL, true, true, true);

        //chart2 = ChartFactory.createCandlestickChart("Default", "X", "Y", xyDataset2, PlotOrientation.VERTICAL);
        
        xyPlot1 = (XYPlot) chart1.getPlot();
        xyPlot2 = (XYPlot) chart2.getPlot();

        dotRenderer1 = xyPlot1.getRenderer();
        dotRenderer1.setSeriesPaint(0, Color.RED);
        ((AbstractRenderer) dotRenderer1).setSeriesShape(0,
                ShapeUtilities.createDiamond(1), true);

        dotRenderer2 = xyPlot2.getRenderer();
        dotRenderer2.setSeriesPaint(0, Color.BLUE);
        ((AbstractRenderer) dotRenderer2).setSeriesShape(0,
                ShapeUtilities.createDiamond(1), true);

        XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer();
        XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer();
        
        renderer1.setSeriesPaint(0, Color.RED);
        renderer1.setSeriesShape(0, ShapeUtilities.createDiamond(0.1f));
        renderer1.setSeriesLinesVisible(0, true);
        
        renderer2.setSeriesPaint(0, Color.BLUE);
        renderer2.setSeriesShape(0, ShapeUtilities.createDiamond(0.1f));
        renderer2.setSeriesLinesVisible(0, true);
        
        xyPlot1.setRenderer(renderer1);
        xyPlot2.setRenderer(renderer2);

        chartpanel1 = new ChartPanel(chart1);
        // chartpanel1.setPreferredSize(new Dimension(500, 250));
        chartpanel1.setSize(500, 250);
        chartpanel1.setAlignmentY(0);
        chartpanel1.setMouseZoomable(true);
        chartpanel1.setMouseWheelEnabled(true);

        chartpanel2 = new ChartPanel(chart2);
        // chartpanel2.setPreferredSize(new Dimension(500, 250));
        chartpanel2.setSize(500, 250);
        chartpanel2.setMouseZoomable(true);
        chartpanel2.setMouseWheelEnabled(true);

        // setContentPane(chartpanel);
        /*
         * this.add(chartpanel1); this.add(chartpanel2);
         */
        jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));

        // jScrollPane = new JScrollPane(jPanel);

        jPanel.add(chartpanel1);
        jPanel.add(chartpanel2);

        // setContentPane(jScrollPane);
        setContentPane(jPanel);
    }
}