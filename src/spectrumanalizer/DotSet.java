package spectrumanalizer;

import java.util.HashSet;
import java.util.Iterator;

public class DotSet extends HashSet<Dot> {

    /**
     * 
     */
    private static final long serialVersionUID = 2563439655229219207L;
    
    public double getMaxX() {
        Iterator<Dot> iter;
        Dot d;
        boolean first = true;
        double max = 0.0d;
        
        iter = this.iterator();
        while (iter.hasNext()) {
            d = iter.next();
            if (first) {
                first = false;
                max = d.getX();
            }
            else {
                if (d.getX() > max) max = d.getX();
            }
        }
        
        return max;
    }
    
    public double getMaxY() {
        Iterator<Dot> iter;
        Dot d;
        boolean first = true;
        double max = 0.0d;
        
        iter = this.iterator();
        while (iter.hasNext()) {
            d = iter.next();
            if (first) {
                first = false;
                max = d.getY();
            }
            else {
                if (d.getY() > max) max = d.getY();
            }
        }
        
        return max;
    }

    public double getMinX() {
        Iterator<Dot> iter;
        Dot d;
        boolean first = true;
        double min = 0.0d;
        
        iter = this.iterator();
        while (iter.hasNext()) {
            d = iter.next();
            if (first) {
                first = false;
                min = d.getX();
            }
            else {
                if (d.getX() < min) min = d.getX();
            }
        }
        
        return min;
    }
    
    public double getMinY() {
        Iterator<Dot> iter;
        Dot d;
        boolean first = true;
        double min = 0.0d;
        
        iter = this.iterator();
        while (iter.hasNext()) {
            d = iter.next();
            if (first) {
                first = false;
                min = d.getY();
            }
            else {
                if (d.getY() < min) min = d.getY();
            }
        }
        
        return min;
    }
    
    public void print() {
        Iterator<Dot> iter = iterator();
        
        while (iter.hasNext()) {
            iter.next().print();
        }
    }
}